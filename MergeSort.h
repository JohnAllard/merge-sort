#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

bool ShowArray(int *,int);
int* MergeSort(int *,int);
int* CopyArray(int *p, int S, int E);

unsigned long int inv = 0;



int* MergeSort(int *p, int L)
{


	if(L == 1 || L == 0) // base case, length 1 or 0 is an already sorted array
		return p; 
	

	int L1 = L/2; int L2 = L-L1;

	int * p1 = CopyArray(p,0,L1);
	int * p2 = CopyArray(p,L1,L);



	p1 = MergeSort(p1,L1);
	p2 = MergeSort(p2,L2);

	
	int INDEX = 0;
	int iii = 0;
	int jjj = 0;

	for(int INDEX = 0;INDEX < L; INDEX++)
	{

		if(iii == L1)
		{
			while(jjj<L2)
			{
				*(p+INDEX) = *(p2+jjj);
				INDEX++;
				jjj++;
			}
			break;
		}
		else if(jjj == L2)
		{
			while(iii<L1)
			{
				*(p+INDEX) = *(p1+iii);
				INDEX++;
				iii++;
			}
			break;
		}


		else if(*(p1+iii) < *(p2+jjj))
		{
			*(p+INDEX) = *(p1+iii);
			iii++;
		}
		else
		{
			*(p+INDEX) = *(p2+jjj);
			jjj++;
			inv += L1-iii;
		}// end else

				
	}// end inner loop

	delete [] p1;
	delete [] p2;

	return p;
}// end function



int* CopyArray(int *p, int S, int E)
{

	int *Hold = new int[E-S];

	for(int iii = 0; iii < (E-S); iii++)
		*(Hold+iii) = *(p+S+iii);

	delete [] p;

	return Hold;
}






bool ShowArray(int *p,int Length)
{
	for(int iii = 0;iii < Length;iii++)
	{
		cout << *(p+iii) << "   ";
		if(iii%5 == 0 && iii != 0)
			cout << endl;
	}
	return true;
}
